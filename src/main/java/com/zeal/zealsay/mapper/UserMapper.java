package com.zeal.zealsay.mapper;

import com.zeal.zealsay.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhanglei
 * @since 2018-09-14
 */
public interface UserMapper extends BaseMapper<User> {

}
