package com.zeal.zealsay.mapper;

import com.zeal.zealsay.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文章表 Mapper 接口
 * </p>
 *
 * @author zhanglei
 * @since 2018-11-28
 */
public interface ArticleMapper extends BaseMapper<Article> {

}
