package com.zeal.zealsay.controller;


import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 文章表 前端控制器
 * </p>
 *
 * @author zhanglei
 * @since 2018-11-28
 */
@Api(tags = "文章模块")
@Slf4j
@Controller
@RequestMapping("/api/v1/article")
public class ArticleController {

}

